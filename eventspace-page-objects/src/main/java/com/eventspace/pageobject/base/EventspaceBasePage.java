package com.eventspace.pageobject.base;

import com.auxenta.framework.BasicPageObject;
import com.auxenta.framework.core.IUIDriver;

public class EventspaceBasePage extends BasicPageObject {

	protected EventspaceBasePage(IUIDriver driver) {
		super(driver);
	}
	
	public String PAGE_TITLE;
	
	public boolean isCorrectPage(){
		return uiDriver.getTitle().equalsIgnoreCase(PAGE_TITLE);
	}
	
	@Deprecated
	public void closeTheBrowser() {
		uiDriver.close();
	}

}
