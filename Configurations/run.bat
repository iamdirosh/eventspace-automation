echo "Installing required libraries";
call mvn install:install-file -Dfile=libs/auxenta-framework/auxtest.framework-1.2.jar -DpomFile=libs/auxenta-framework/pom.xml
call mvn install:install-file -Dfile=libs/ojdbc/ojdbc14-10.2.0.4.0.jar -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.4.0 -Dpackaging=jar

replace ".\libs\pom.xml" "%0\..\.."
cd ..
call mvn eclipse:eclipse