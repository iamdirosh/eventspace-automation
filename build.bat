@echo off
rem set rev=
set suite=
rem set /p rev="Please Provide the revision number to download, or leave blank for latest: "
set /p suite="Please Provide the suite name to execute, or leave blank for default:"
echo 
rem if %rev%.==. echo Revision not provided, taking the latest revision.
rem if not %rev%.==. echo Checking out the Revision %rev%
if %suite%.==. echo Test suite not provided, Executing the default Smoke suite.
if not %suite%.==. echo Executing the test suite %suite%
echo 

@echo on
call git pull
call Configurations\run.bat
